import {Directive, ElementRef, Input, OnInit} from '@angular/core';

@Directive({
  selector: '[appBackgroundHome]'
})
export class BackgroundHomeDirective implements OnInit{

  @Input('appBackgroundHome') bckground : string;
  constructor(private el:ElementRef) {

  }

  ngOnInit() {
    this.el.nativeElement.style.background= this.bckground;
  }

}
