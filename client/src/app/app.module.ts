import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';

import { AppComponent } from './app.component';
import {NavbarComponent } from './components/navbar/navbar.component';
import { ConnexionComponent } from './components/connexion/connexion.component';
import { InscriptionComponent } from './components/inscription/inscription.component';
import {routes} from './app.router';
import { HomeComponent } from './components/home/home.component';

// Import du module Service
import { DataService} from './services/data.service'


//Material design modules
import  {
  MatButtonModule, MatInputModule, MatSelectModule, MatDatepickerModule, MatNativeDateModule,
  MatCardModule
} from '@angular/material';
import {MatToolbarModule} from '@angular/material/toolbar';
import {MatFormFieldModule} from '@angular/material/form-field';
import {MatSidenavModule} from '@angular/material/sidenav';
import {MatListModule} from '@angular/material/list';


//forms
import {ReactiveFormsModule} from '@angular/forms';
//icons
import {MatIconModule} from '@angular/material/icon';
import {MatCheckboxModule} from '@angular/material/checkbox';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {MatRadioModule} from '@angular/material/radio';
import {MatSnackBarModule} from '@angular/material/snack-bar';
import {UsersService} from "./services/users/users.service";
import { SupprimerCompteComponent } from './components/administrateur/supprimer-compte/supprimer-compte.component';
import {BackgroundHomeDirective} from "./directives/background-home/background-home.directive";
import { AjoutTrajetComponent } from './components/administrateur/ajout-trajet/ajout-trajet.component';
import { StatistiquesComponent } from './components/statistiques/statistiques.component';
import { ProposerTrajetComponent } from './components/membres/proposer-trajet/proposer-trajet.component';
import {FlashMessagesModule} from "angular2-flash-messages";
import {AuthService} from "./services/Authentification/auth.service";
import { ProfileComponent } from './components/profile/profile/profile.component';
import {AuthGuard} from "./guards/auth.guard";
import {MatTableModule} from '@angular/material/table';
import { MesTrajetsComponent } from './components/membres/mes-trajets/mes-trajets.component';
import {MatButtonToggleModule} from '@angular/material/button-toggle';
import { TrajetsParticipesMembresComponent } from './components/membres/trajets-participes-membres/trajets-participes-membres.component';
import { TrajetsProposesMembresComponent } from './components/membres/trajets-proposes-membres/trajets-proposes-membres.component';
import { TrajetsChoixMembresComponent } from './components/membres/trajets-choix-membres/trajets-choix-membres.component';



@NgModule({
  declarations: [
    AppComponent,
    NavbarComponent,
    ConnexionComponent,
    InscriptionComponent,
    HomeComponent,
    SupprimerCompteComponent,
    BackgroundHomeDirective,
    AjoutTrajetComponent,
    StatistiquesComponent,
    ProposerTrajetComponent,
    ProfileComponent,
    MesTrajetsComponent,
    TrajetsParticipesMembresComponent,
    TrajetsProposesMembresComponent,
    TrajetsChoixMembresComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    routes,
    MatButtonModule,
    MatToolbarModule,
    MatFormFieldModule,
    MatInputModule,
    MatSelectModule,
    ReactiveFormsModule,
    MatIconModule,
    MatCheckboxModule,
    MatDatepickerModule,
    MatNativeDateModule,
    BrowserAnimationsModule,
    MatRadioModule,
    MatSnackBarModule,
    MatSidenavModule,
    MatListModule,
    FlashMessagesModule.forRoot(),
    MatTableModule,
    MatCardModule,
    MatButtonToggleModule



],
  providers: [DataService, UsersService,AuthService,AuthGuard],
  bootstrap: [AppComponent]
})
export class AppModule { }
