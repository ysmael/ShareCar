import { Component, OnInit } from '@angular/core';
import {FormControl, Validators} from '@angular/forms';
import {User} from '../../class/User';
import {FlashMessagesService} from "angular2-flash-messages";
import {Router} from "@angular/router";
import {AuthService} from "../../services/Authentification/auth.service";
import {MatSnackBar} from "@angular/material";


@Component({
  selector: 'app-connexion',
  templateUrl: './connexion.component.html',
  styleUrls: ['./connexion.component.css'],
  providers:[AuthService,FlashMessagesService]
})
export class ConnexionComponent implements OnInit {
email:string;
 mdp:string;

  constructor(
      public snackBar: MatSnackBar,
      private authService:AuthService,
      private flashMessage:FlashMessagesService,
      private router:Router
  ) { }

  ngOnInit() {
  }


    openSnackBar(message: string, action: string) {
        this.snackBar.open(message, action, {
            duration: 3500,
        });
    }

  hide = true;
  emailcontrol = new FormControl('', [Validators.required, Validators.email]);

  getErrorMessage() {
    return this.emailcontrol.hasError('required') ? 'Vous devez entrez un email' :
        this.emailcontrol.hasError('email') ? 'Email non valide' :
            '';
  }

  login(){

    const user ={
      email:this.email,
      mdp:this.mdp
    };
 this.authService.authentificateUser(user).subscribe(data=>{
     if(data.isSuccess){
    this.authService.storeUserData(data.token, data.user);

    this.openSnackBar("Connection réussi.","");

         this.router.navigate(['home']);
     }
     else {
         this.openSnackBar(data.message,"");
    this.router.navigate(['connexion']);
     }
 });
 //console.log("login(event) appelle");
  }
}
