import { Component, OnInit } from '@angular/core';
import {UsersService} from "../../../services/users/users.service";
import {User} from '../../../class/User';
import {MatSnackBar} from "@angular/material";


@Component({
  selector: 'app-supprimer-compte',
  templateUrl: './supprimer-compte.component.html',
  styleUrls: ['./supprimer-compte.component.css'],
  providers:[UsersService]
})
export class SupprimerCompteComponent implements OnInit {
users:User[];

  id:string;
  constructor(public snackBar: MatSnackBar,private usersService: UsersService) {
    this.usersService.getMembres().subscribe(users=>this.users=users);
  }

  ngOnInit() {
  }

  openSnackBar(message: string, action: string) {
    this.snackBar.open(message, action, {
      duration: 3500,
    });
  }

  supprimerDefinitivement(userId,index) {
    const user={
      id:userId
    };

    this.usersService.supprimerD(user).subscribe(data => {
      if (data.isSuccess) {
        this.openSnackBar(data.message, "");

      } else {
        this.openSnackBar(data.message, "");
      }
    });
  }

  supprimerTemporairement(userId){
    const user={
      id:userId
    };
    this.usersService.supprimerTemp(user).subscribe(data => {
      if (data.isSuccess) {
        this.openSnackBar(data.message, "");
      } else {
        this.openSnackBar(data.message, "");
      }
    });
  }
}
