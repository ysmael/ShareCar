import { Component, OnInit } from '@angular/core';
import {TrajetsService} from "../../../services/trajets/trajets.service";
import {Trajet} from '../../../class/Trajet';
import {MatTableDataSource, MatSnackBar} from "@angular/material";

@Component({
  selector: 'app-ajout-trajet',
  templateUrl: './ajout-trajet.component.html',
  styleUrls: ['./ajout-trajet.component.css'],
  providers:[TrajetsService]
})
export class AjoutTrajetComponent implements OnInit {

  trajets:Trajet[];
  depart:string;
  destination:string;
  distance:string;
  tempsMoyen:string;
  prixReference:number;



  constructor(private trajetsService: TrajetsService,  public snackBar: MatSnackBar) {
    this.trajetsService.getTrajets().subscribe(trajets=>this.trajets=trajets);


  }

  ngOnInit() {

  }


  openSnackBar(message: string, action: string) {
    this.snackBar.open(message, action, {
      duration: 3500,
    });
  }

  addTrajet(){



    var newTrajet={
      depart:this.depart,
    destination:this.destination,
    distance:this.distance,
    tempsMoyen:this.tempsMoyen,
      prixReference:this.prixReference
    };

    this.trajetsService.addTrajet(newTrajet).subscribe(trajet=> {
      if (trajet.isSuccess) {
        this.openSnackBar(trajet.message, "");
        this.trajets=[];
        this.getTrajet();
      } else {
        this.openSnackBar(trajet.message, "");
      }
    });


  }

  getTrajet(){
    this.trajetsService.getTrajets().subscribe(trajets=>this.trajets=trajets);
  }

  supprimerTrajet(depart,destination, distance, tempsMoyen, index){
    var requete={
      depart:depart,
      destination:destination,
      distance:distance,
      tempsMoyen:tempsMoyen
    };
    console.log(requete);
    this.trajetsService.supprimerTrajet(requete).subscribe(data => {
      if (data.isSuccess) {
        this.openSnackBar(data.message, "");
        this.trajets.splice(index,1);
      } else {
        this.openSnackBar(data.message, "");
      }
    });
  }


}
