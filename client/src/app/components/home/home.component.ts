import {Component, OnInit, OnChanges} from '@angular/core';
import {DataService} from "../../services/data.service";
import {TrajetsMembreService} from "../../services/trajetsMembre/trajets-membre.service";
import {TrajetMembre} from  '../../class/TrajetMembre'
import {AuthService} from "../../services/Authentification/auth.service";
import {User} from '../../class/User'
import {MatSnackBar} from "@angular/material";


@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css'],
  providers:[TrajetsMembreService, AuthService]
})
export class HomeComponent implements OnInit, OnChanges{
depart:string;
destination:string;
date: Date;
  trajetsMembre:TrajetMembre[];
  user:User;
  private srcBackground: string ="assets/images/header2.jpg";
  private srcCardImage:string="assets/images/inspirationvoyage.jpg";

  constructor(
      private  trajetsMembreService:TrajetsMembreService,
      private authService:AuthService,
      public snackBar: MatSnackBar
  ) {
    this.authService.getProfile().subscribe(profile => {
          this.user = profile.user;
          console.log(this.user._id);
        },
        err => {
          console.log(err);
          return false;
        });
    ;
  }

  ngOnInit() {
  }
  ngOnChanges(){

  }

  openSnackBar(message: string, action: string) {
    this.snackBar.open(message, action, {
      duration: 3500,
    });
  }

//date: { "$lte": new Date(Date.now()) },
  getTrajetMembre(){
    const trajetRequest ={
      depart:this.depart,
      destination:this.destination,
      date:this.date,

    };
    console.log(trajetRequest);

    this.trajetsMembreService.getTrajetMembre(trajetRequest).subscribe(trajets => {


    console.log( "GET TRAJET : "+trajets);
      this.trajetsMembre=trajets;
    });
  }

addParticipant(idTrajet){
    var requete={
      idTrajet:idTrajet,
      participant:this.user
    };
    console.log(requete);
    this.trajetsMembreService.addParticipant(requete).subscribe(data => {
      if (data.isSuccess) {
        this.openSnackBar(data.message, "");
      } else {
        this.openSnackBar(data.message, "");
      }
    });
}
}
