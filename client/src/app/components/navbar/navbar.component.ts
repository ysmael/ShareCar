import {Component, OnInit, OnChanges} from '@angular/core';
import {Router} from "@angular/router";
import {FlashMessagesService} from "angular2-flash-messages";
import {AuthService} from "../../services/Authentification/auth.service";
import {MatSnackBar} from "@angular/material";

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent implements OnInit , OnChanges{

  constructor(
      public snackBar: MatSnackBar,
      private authService:AuthService,
      private flashMessage:FlashMessagesService,
      private router:Router
  ) {
      this.authService.checkProfilUser();
  }

  ngOnInit() {

  }

  ngOnChanges(){
      this.authService.checkProfilUser();
  }

    openSnackBar(message: string, action: string) {
        this.snackBar.open(message, action, {
            duration: 3500,
        });
    }

  onLogoutClick(){
this.authService.logout();
this.openSnackBar("Déconnexion réussi","");
    this.router.navigate(['connexion']);
    return false;
  }
}
