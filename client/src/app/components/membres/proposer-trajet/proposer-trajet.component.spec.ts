import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProposerTrajetComponent } from './proposer-trajet.component';

describe('ProposerTrajetComponent', () => {
  let component: ProposerTrajetComponent;
  let fixture: ComponentFixture<ProposerTrajetComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProposerTrajetComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProposerTrajetComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
