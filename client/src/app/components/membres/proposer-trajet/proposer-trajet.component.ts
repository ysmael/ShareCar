import { Component, OnInit } from '@angular/core';
import {TrajetsMembreService} from "../../../services/trajetsMembre/trajets-membre.service";
import {MatSnackBar} from "@angular/material";
import {AuthService} from "../../../services/Authentification/auth.service";
import {ISO8601_DATE_REGEX} from "@angular/common/src/pipes/date_pipe";
import {TrajetsService} from "../../../services/trajets/trajets.service";
import {Trajet} from '../../../class/Trajet';

@Component({
  selector: 'app-proposer-trajet',
  templateUrl: './proposer-trajet.component.html',
  styleUrls: ['./proposer-trajet.component.css'],
  providers:[TrajetsMembreService,AuthService, TrajetsService]
})
export class ProposerTrajetComponent implements OnInit {
  trajets:Trajet[];
  depart:string;
  destination:string;
  date :Date;
  adresseDepart:string;
  adresseArrivee:string;
  typeVoiture:string;
  nbPlace: string;
  prix:number;
  idMembre:string;
  nomMembre:string;
  prenomMembre:string;
  sexeMembre:string;
  emailMembre:string;
  dateNaissanceMembre:Date;
  hide=false;
  textbtn="Voir type trajet";
  private srcBackground: string ="assets/images/header.jpg";

  constructor(public snackBar: MatSnackBar,
              private trajetsMembreService : TrajetsMembreService,
              private trajetsService: TrajetsService,
  private authService:AuthService) {
  this.trajetsService.getTrajets().subscribe(trajets=>this.trajets=trajets);
  }

  ngOnInit() {
  }

  openSnackBar(message: string, action: string) {
    this.snackBar.open(message, action , {
      duration: 5000,

    });
  }

  addTrajetMembre(){



    var newTrajetMembre={
      depart:this.depart,
      destination:this.destination,
      date :this.date,
    adresseDepart:this.adresseDepart,
    adresseArrivee:this.adresseArrivee,
    typeVoiture:this.typeVoiture,
    nbPlace: this.nbPlace,
    prix:this.prix,
    idMembre:this.authService.idUser,
      nomMembre:this.authService.nomUser,
      prenomMembre:this.authService.prenomUser,
      sexeMembre:this.authService.sexeUser,
      emailMembre:this.authService.emailUser,
      dateNaissanceMembre:this.authService.dateNaissanceUser
    };

    console.log(newTrajetMembre);
    this.trajetsMembreService.addTrajetMembre(newTrajetMembre).subscribe(data => {
      if(data.isSuccess){
        this.openSnackBar(data.message,"");
      } else {
        this.openSnackBar(data.message,"");
      }
    });

  }

  voirTab(){
    if (!this.hide){
      this.hide=true;
      this.textbtn="Cachez type trajet";
    }
    else {
      this.hide=false;
      this.textbtn="Voir type trajet";
    }
  }
}
