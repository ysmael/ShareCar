import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TrajetsParticipesMembresComponent } from './trajets-participes-membres.component';

describe('TrajetsParticipesMembresComponent', () => {
  let component: TrajetsParticipesMembresComponent;
  let fixture: ComponentFixture<TrajetsParticipesMembresComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TrajetsParticipesMembresComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TrajetsParticipesMembresComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
