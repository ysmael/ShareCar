import { Component, OnInit } from '@angular/core';
import {AuthService} from "../../../services/Authentification/auth.service";
import {TrajetsMembreService} from "../../../services/trajetsMembre/trajets-membre.service";
import {TrajetMembre} from  '../../../class/TrajetMembre';
import {User} from '../../../class/User';
import {MatSnackBar} from "@angular/material";


@Component({
  selector: 'app-trajets-participes-membres',
  templateUrl: './trajets-participes-membres.component.html',
  styleUrls: ['./trajets-participes-membres.component.css'],
  providers:[AuthService,TrajetsMembreService]
})
export class TrajetsParticipesMembresComponent implements OnInit {

  trajetsMembre:TrajetMembre[];
  user:User;
  idMembre:string;
  userId:string;
  private srcCardImage:string="assets/images/inspirationvoyage.jpg";


  constructor(private  trajetsMembreService:TrajetsMembreService,
              private authService:AuthService,
              public snackBar: MatSnackBar) {

    this.authService.getProfile().subscribe(profile => {
          this.user = profile.user;

          this.userId=this.user._id;
          this.getTrajetParticipe(this.userId);
          console.log("useridparticipé"+this.userId);
        },
        err => {
          console.log(err);
          return false;
        });
    ;

  }

  ngOnInit() {

  }

  openSnackBar(message: string, action: string) {
    this.snackBar.open(message, action, {
      duration: 3500,
    });
  }

  getTrajetParticipe(_user) {
    console.log("gettratjet participé  "+ this.userId);
    const user ={
      idMembre:_user
    };

    this.trajetsMembreService.getTrajetParticipeOfUser(user).subscribe(trajets => {


      console.log( "GET TRAJET PARTICIPE : "+trajets);
      this.trajetsMembre=trajets;
    });
  }


  supprimerParticipation(idTrajet, index){
    var requete={
      idTrajet:idTrajet,
      idParticipant:this.userId
    };
    console.log(requete);
    this.trajetsMembreService.supprimerParticipation(requete).subscribe(data => {
      if (data.isSuccess) {
        this.openSnackBar(data.message, "");
        this.trajetsMembre.splice(index,1);
      } else {
        this.openSnackBar(data.message, "");
      }
    });
  }


}
