import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TrajetsProposesMembresComponent } from './trajets-proposes-membres.component';

describe('TrajetsProposesMembresComponent', () => {
  let component: TrajetsProposesMembresComponent;
  let fixture: ComponentFixture<TrajetsProposesMembresComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TrajetsProposesMembresComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TrajetsProposesMembresComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
