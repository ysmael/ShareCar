import { Component, OnInit } from '@angular/core';
import {TrajetsMembreService} from "../../../services/trajetsMembre/trajets-membre.service";
import {TrajetMembre} from  '../../../class/TrajetMembre';
import {User} from '../../../class/User';
import {AuthService} from "../../../services/Authentification/auth.service";
import {MatSnackBar} from "@angular/material";
@Component({
  selector: 'app-trajets-proposes-membres',
  templateUrl: './trajets-proposes-membres.component.html',
  styleUrls: ['./trajets-proposes-membres.component.css'],
  providers:[AuthService,TrajetsMembreService]
})
export class TrajetsProposesMembresComponent implements OnInit {
  trajetsMembre:TrajetMembre[];
  user:User;
  idMembre:string;
  userId:string;
  private srcCardImage:string="assets/images/inspirationvoyage.jpg";

  constructor(
      private  trajetsMembreService:TrajetsMembreService,
      private authService:AuthService,
      public snackBar: MatSnackBar
  )
  {

    this.authService.getProfile().subscribe(profile => {
          this.user = profile.user;

          this.userId=this.user._id;
          this.getTrajetPropose(this.userId);
          console.log("userid"+this.userId);
        },
        err => {
          console.log(err);
          return false;
        });
    ;


  }

  ngOnInit() {



  }

  openSnackBar(message: string, action: string) {
    this.snackBar.open(message, action, {
      duration: 3500,
    });
  }
  getTrajetPropose(_user) {
    console.log("gettratjet   "+ this.userId);
    const user ={
      idMembre:_user
    };

    this.trajetsMembreService.getTrajetMembreOfUser(user).subscribe(trajets => {


      console.log( "GET TRAJET : "+trajets);
      this.trajetsMembre=trajets;
    });
  }

  supprimerTrajet(trajetId,index){
    const trajet ={
      idTrajet:trajetId
    };

    this.trajetsMembreService.supprimerTrajet(trajet).subscribe(data => {
      if (data.isSuccess) {
        this.openSnackBar(data.message, "");
        this.trajetsMembre.splice(index,1);
      } else {
        this.openSnackBar(data.message, "");
      }
    });
  }

}
