import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TrajetsChoixMembresComponent } from './trajets-choix-membres.component';

describe('TrajetsChoixMembresComponent', () => {
  let component: TrajetsChoixMembresComponent;
  let fixture: ComponentFixture<TrajetsChoixMembresComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TrajetsChoixMembresComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TrajetsChoixMembresComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
