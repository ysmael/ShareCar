import { Component, OnInit } from '@angular/core';
import {FormControl, Validators} from '@angular/forms';
import {MatSnackBar} from '@angular/material';
import {User} from '../../class/User';
import {AuthService} from "../../services/Authentification/auth.service";
import {FlashMessagesService} from "angular2-flash-messages";
import {Router} from "@angular/router";
import {ValidateService} from "../../services/Authentification/validate.service";

@Component({
  selector: 'app-inscription',
  templateUrl: './inscription.component.html',
  styleUrls: ['./inscription.component.css'],
  providers:[AuthService,FlashMessagesService,ValidateService]
})
export class InscriptionComponent implements OnInit {
users:User[];
  nom:string;
  sexe:string;
  prenom:string;
  email:string;
  mdp:string;
  dateNaissance :Date;

  constructor(public snackBar: MatSnackBar,
              private authService:AuthService,
  private  flashMessage: FlashMessagesService,
              private router:Router,
              private validateService: ValidateService



  ) {
  }

  ngOnInit() {
  }

  openSnackBar(message: string, action: string) {
    this.snackBar.open(message, action, {
      duration: 3500,
    });
  }

  emailcontrol = new FormControl('', [Validators.required, Validators.email]);

  getErrorMessage() {
    return this.emailcontrol.hasError('required') ? 'Vous devez entrez un email' :
        this.emailcontrol.hasError('email') ? 'Email non valide' :
            '';
  }

  myFilter = (d: Date): boolean => {
    const day = d.getDay();
    // Prevent Saturday and Sunday from being selected.
    return day !== 0 && day !== 6;
  }



  addUser(){


    const user={
      nom:this.nom,
      prenom:this.prenom,
      dateNaissance:this.dateNaissance,
      email:this.email,
      mdp:this.mdp,
      sexe:this.sexe,
      "profil":"membre"
    };

    // Verif de tous les champs
    if(!this.validateService.validateRegister(user)){
this.openSnackBar("Touts les champs doivent être remplis","");

      return false;
    }

    // Verif Email
    if(!this.validateService.validateEmail(user.email)){
      this.openSnackBar("Veuillez utiliser un email valide s\' il vous plait","");
     return false;
    }


  this.authService.registerUser(user).subscribe(data => {
  if(data.isSuccess){
    this.openSnackBar("Vous êtes maintenant inscrit. Bienvenue parmi la communauté ShareCar. Vous pouvez maintenant vous connectez","");
    this.router.navigate(['/connexion']);
  } else {
    this.openSnackBar("Quelque chose s\'est mal passé","");
    this.router.navigate(['/inscription']);
  }
});
  }

}
