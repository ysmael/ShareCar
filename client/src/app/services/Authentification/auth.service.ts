import { Injectable } from '@angular/core';
import {RequestOptions, Http, Headers} from "@angular/http";
import {tokenNotExpired} from 'angular2-jwt';
import 'rxjs/add/operator/map';
import {User} from '../../class/User'
@Injectable()
export class AuthService {
  authToken: any;
  user:User;
  isAdmin:boolean;

  isMembre:boolean;
  isInvite:boolean;
  idUser:string;
  nomUser:string;
  prenomUser:string;
  sexeUser:string;
  emailUser:string;
  dateNaissanceUser:Date;

  constructor(private _http:Http) {
    console.log(this.isAdmin);
    console.log(this.isMembre);
    console.log(this.isInvite);
    this.checkProfilUser();
    console.log(this.isAdmin);
    console.log(this.isMembre);
    console.log(this.isInvite);
  }


  registerUser(newUser){
    console.log(newUser);
    var headers = new Headers();
    headers.append('Content-Type', 'application/json');
    let options = new RequestOptions({ headers: headers });
    return this._http.post("http://localhost:3000/api/users/inscription", JSON.stringify(newUser), options).map(res => res.json());

  }


  authentificateUser(user){
    var headers = new Headers();
    headers.append('Content-Type', 'application/json');
    let options = new RequestOptions({ headers: headers });
    return this._http.post("http://localhost:3000/api/users/connection", JSON.stringify(user), options).map(res => res.json());
  }

  storeUserData(token, user){
    localStorage.setItem('id_token', token);
    localStorage.setItem('user', JSON.stringify(user));

    this.authToken=token;
    this.user = user;
    this.idUser=this.user._id;
    this.nomUser=this.user.nom;
    this.prenomUser=this.user.prenom;
    this.sexeUser=this.user.sexe;
    this.emailUser=this.user.email;
    this.dateNaissanceUser=this.user.dateNaissance;

    console.log("USERSTORE"+ this.authToken + "   "+this.user.profil);
    if(this.user.profil=="administrateur"){
      this.isAdmin=true;
      console.log("Administrateur true");
    }
    else if (this.user.profil=="membre") {
      this.isMembre=true;
      console.log("Membre "+ true);
    }
    else {
      this.isInvite=true;
      console.log("Invité true");
    }

  }

  logout(){
    this.authToken= null;
    this.user=null;
    localStorage.clear();
  }

  checkProfilUser(){
    if(this.loggedIn()) {
      const localuser = localStorage.getItem('user');
      this.user=JSON.parse(localuser);
console.log(localuser);
      if(this.user.profil=="administrateur"){
        this.isAdmin=true;
        this.idUser=this.user._id;
        this.nomUser=this.user.nom;
        this.prenomUser=this.user.prenom;
        this.sexeUser=this.user.sexe;
        this.emailUser=this.user.email;
        this.dateNaissanceUser=this.user.dateNaissance;
        console.log("Administrateur true");
      }
      else if (this.user.profil=="membre") {
        this.isMembre=true;
        this.idUser=this.user._id;
        this.nomUser=this.user.nom;
        this.prenomUser=this.user.prenom;
        this.sexeUser=this.user.sexe;
        this.emailUser=this.user.email;
        this.dateNaissanceUser=this.user.dateNaissance;
        console.log("Membre "+ this.isMembre + " id :" +this.idUser );
      }
      else {
        this.isInvite=true;
        this.idUser=this.user._id;
        this.nomUser=this.user.nom;
        this.prenomUser=this.user.prenom;
        this.sexeUser=this.user.sexe;
        this.emailUser=this.user.email;
        this.dateNaissanceUser=this.user.dateNaissance;
        console.log("Invité true");
      }
    }
  }




  getProfile(){
    let headers = new Headers();
    this.loadToken();
    headers.append('Authorization', this.authToken);
    headers.append('Content-Type','application/json');
    return this._http.get("http://localhost:3000/api/users/profile",{headers: headers})
        .map(res => res.json());
  }
  loadToken(){
    const token = localStorage.getItem('id_token');
    this.authToken = token;
  }

  loggedIn(){
    return tokenNotExpired('id_token');
  }


    getIdUser(){
    if(this.loggedIn()){
  return this.user._id;}
  }

}
