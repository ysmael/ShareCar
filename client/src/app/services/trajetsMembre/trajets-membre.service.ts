import { Injectable } from '@angular/core';
import {Http, Headers, RequestOptions} from "@angular/http";

@Injectable()
export class TrajetsMembreService {

  constructor(private _http:Http) {
    console.log("TrajetsMembreServices initialisé");
  }



  getTrajetsMembre(){
    return this._http.get("http://localhost:3000/api/trajetsMembre")
        .map(result => result.json().data);
  }

  addTrajetMembre(newTrajetMembre){
    console.log(newTrajetMembre);
    var headers = new Headers();
    headers.append('Content-Type', 'application/json');
    let options = new RequestOptions({ headers: headers });
    return this._http.post("http://localhost:3000/api/trajetsMembre/ajout", JSON.stringify(newTrajetMembre), options).map(res => res.json());

  }

  getTrajetMembre(trajetRequest){
    var headers = new Headers();
    headers.append('Content-Type', 'application/json');
    let options = new RequestOptions({ headers: headers });
    return this._http.post("http://localhost:3000/api/trajetsMembre/getTrajet", JSON.stringify(trajetRequest), options).map(res => res.json());
  }


  getTrajetMembreOfUser(user){
    var headers = new Headers();
    headers.append('Content-Type', 'application/json');
    let options = new RequestOptions({ headers: headers });
    return this._http.post("http://localhost:3000/api/trajetsMembre/getTrajetPropose", JSON.stringify(user), options).map(res => res.json());
  }

  addParticipant(requete){
    var headers = new Headers();
    headers.append('Content-Type', 'application/json');
    let options = new RequestOptions({ headers: headers });

    return this._http.post("http://localhost:3000/api/trajetsMembre/addParticipant", JSON.stringify(requete), options).map(res => res.json());
  }

  getTrajetParticipeOfUser(user){
    var headers = new Headers();
    headers.append('Content-Type', 'application/json');
    let options = new RequestOptions({ headers: headers });
    return this._http.post("http://localhost:3000/api/trajetsMembre/getTrajetParticipe", JSON.stringify(user), options).map(res => res.json());
  }

  supprimerTrajet(trajet){
    var headers = new Headers();
    headers.append('Content-Type', 'application/json');
    let options = new RequestOptions({ headers: headers });
    return this._http.post("http://localhost:3000/api/trajetsMembre/supprimerTrajet",JSON.stringify(trajet),options).map(res=>res.json());
  }


  supprimerParticipation(requete){
    var headers = new Headers();
    headers.append('Content-Type', 'application/json');
    let options = new RequestOptions({ headers: headers });

    return this._http.post("http://localhost:3000/api/trajetsMembre/supprimerParticipation", JSON.stringify(requete), options).map(res => res.json());
  }


}
