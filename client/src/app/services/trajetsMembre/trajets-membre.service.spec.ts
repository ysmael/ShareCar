import { TestBed, inject } from '@angular/core/testing';

import { TrajetsMembreService } from './trajets-membre.service';

describe('TrajetsMembreService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [TrajetsMembreService]
    });
  });

  it('should ...', inject([TrajetsMembreService], (service: TrajetsMembreService) => {
    expect(service).toBeTruthy();
  }));
});
