import { Injectable } from '@angular/core';
import {RequestOptions, Headers, Http} from "@angular/http";

@Injectable()
export class TrajetsService {

  constructor(private _http:Http) {
    console.log("TrajetsServices initialisé");
  }


  getTrajets(){
    return this._http.get("http://localhost:3000/api/trajets")
        .map(result => result.json());
  }

  addTrajet(newTrajet){
    console.log(newTrajet);
    var headers = new Headers();
    headers.append('Content-Type', 'application/json');
    let options = new RequestOptions({ headers: headers });
    return this._http.post("http://localhost:3000/api/trajets/ajout", JSON.stringify(newTrajet), options).map(res => res.json());

  }


  getByDepartDestination(trajet){
    var headers = new Headers();
    headers.append('Content-Type', 'application/json');
    let options = new RequestOptions({ headers: headers });
    return this._http.post("http://localhost:3000/api/trajets/getByDepartDestination",JSON.stringify(trajet),options).map(res=>res.json());
  }

  updateTrajet(trajet){
    var headers = new Headers();
    headers.append('Content-Type', 'application/json');
    let options = new RequestOptions({ headers: headers });
    return this._http.post("http://localhost:3000/api/trajets/update",JSON.stringify(trajet),options).map(res=>res.json());
  }

  supprimerTrajet(trajet){
      var headers = new Headers();
      headers.append('Content-Type', 'application/json');
      let options = new RequestOptions({ headers: headers });
      return this._http.post("http://localhost:3000/api/trajets/delete",JSON.stringify(trajet),options).map(res=>res.json());
  }

}


