import { Injectable } from '@angular/core';
import { Http, Headers, RequestOptions } from '@angular/http';
import 'rxjs/add/operator/map';
import {map} from "rxjs/operator/map";

@Injectable()
export class UsersService {



  constructor(private _http:Http) {
console.log("UsersServices initialisé");
  }


  getUsers(){
    return this._http.get("http://localhost:3000/api/users")
        .map(result => result.json().data);
  }




  getMembres(){
    return this._http.get("http://localhost:3000/api/users/membres")
        .map(result => result.json());
  }

  supprimerD(user){
    var headers = new Headers();
    headers.append('Content-Type', 'application/json');
    let options = new RequestOptions({ headers: headers });
    return this._http.post("http://localhost:3000/api/users/membres/supprimerD", JSON.stringify(user), options).map(res => res.json());
  }

  supprimerTemp(user){
    var headers = new Headers();
    headers.append('Content-Type', 'application/json');
    let options = new RequestOptions({ headers: headers });
    return this._http.post("http://localhost:3000/api/users/membres/supprimerT", JSON.stringify(user), options).map(res => res.json());
  }
}
