import {ModuleWithProviders } from '@angular/core';
import {Routes, RouterModule, ROUTER_CONFIGURATION} from '@angular/router';


import { AppComponent } from './app.component';
import {NavbarComponent } from './components/navbar/navbar.component';
import { ConnexionComponent } from './components/connexion/connexion.component';
import { InscriptionComponent } from './components/inscription/inscription.component';
import {HomeComponent} from "./components/home/home.component";
import {AjoutTrajetComponent} from "./components/administrateur/ajout-trajet/ajout-trajet.component";
import {SupprimerCompteComponent} from "./components/administrateur/supprimer-compte/supprimer-compte.component";
import {StatistiquesComponent} from "./components/statistiques/statistiques.component";
import {ProfileComponent} from "./components/profile/profile/profile.component";
import {AuthGuard} from "./guards/auth.guard"
import {ProposerTrajetComponent} from "./components/membres/proposer-trajet/proposer-trajet.component";
import {MesTrajetsComponent} from "./components/membres/mes-trajets/mes-trajets.component";
import {TrajetsParticipesMembresComponent} from "./components/membres/trajets-participes-membres/trajets-participes-membres.component";
import {TrajetsProposesMembresComponent} from "./components/membres/trajets-proposes-membres/trajets-proposes-membres.component";
import {TrajetsChoixMembresComponent} from "./components/membres/trajets-choix-membres/trajets-choix-membres.component";
export  const  router: Routes = [
    { path: '', redirectTo: 'home', pathMatch: 'full' },
    {path: 'connexion', component:ConnexionComponent},
    {path: 'inscription', component:InscriptionComponent},
    {path: 'home', component:HomeComponent},
    {path: 'ajoutTrajet', component:AjoutTrajetComponent, canActivate:[AuthGuard]},
    {path: 'supprimerCompte', component:SupprimerCompteComponent, canActivate:[AuthGuard]},
    {path: 'statistiques', component:StatistiquesComponent, canActivate:[AuthGuard]},
    {path: 'profile', component:ProfileComponent, canActivate:[AuthGuard]},
    {path: 'proposerTrajet', component:ProposerTrajetComponent, canActivate:[AuthGuard]},
    {path: 'mestrajetsmembre', component:MesTrajetsComponent,children:[
        {
            path:'',
            component:TrajetsChoixMembresComponent
        },
        {
            path:'trajetsparticipes',
            component:TrajetsParticipesMembresComponent
        },
        {
            path:'trajetsproposes',
            component:TrajetsProposesMembresComponent
        }
    ], canActivate:[AuthGuard]}
];

export const routes: ModuleWithProviders = RouterModule.forRoot(router);
