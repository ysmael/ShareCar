export class TrajetMembre{
    depart:string;
    destination:string;
    date :Date;
    adresseDepart:string;
    adresseArrivee:string;
    typeVoiture:string;
    nbPlace: string;
    prix:number;
    idMembre:string;
    nomMembre:string;
    prenomMembre:string;
    sexeMembre:string;
    emailMembre:string;
    dateNaissanceMembre: Date;
    participants : [];
    nbPlaceDisponible:string;

}