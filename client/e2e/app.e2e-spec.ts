import { ShareCarPage } from './app.po';

describe('share-car App', () => {
  let page: ShareCarPage;

  beforeEach(() => {
    page = new ShareCarPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
