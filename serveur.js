const express = require('express');
const bodyParser = require('body-parser');
const path = require('path');
const http = require('http');
const app = express();
const cors = require('cors');
const passport = require('passport');

// API file for interacting with MongoDB
const api = require('./serveur/routes/api');
const users = require('./serveur/routes/users');
const index = require('./serveur/routes/index');
const trajets= require ('./serveur/routes/trajets');
const trajetsMembre= require ('./serveur/routes/trajetsMembre');


const mongoose = require('mongoose');
const config = require('./serveur/config/database');



// Connect To Database
mongoose.connect(config.database);

// On Connection
mongoose.connection.on('connected', () => {
    console.log('Connected to database '+config.database);
});

// On Error
mongoose.connection.on('error', (err) => {
    console.log('Database error: '+err);
});


//pour les problemes de permissions
app.use(cors());

//passport middleware
app.use(passport.initialize());
app.use(passport.session());

require('./serveur/config/passport')(passport);

// Parsers
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false}));




// API location
app.use('/api', api);
//users
app.use('/api',users);
//trajets
app.use('/api',trajets);
//trajetsMembre
app.use('/api', trajetsMembre);

app.use(express.static(path.join(__dirname, 'client/src')));

app.set('views', path.join(__dirname, 'client'));
app.set('view engine', 'ejs');
app.engine('html', require('ejs').renderFile);



// Evoyer toutes les autres requetes à Angular app
app.get('/', function(req, res){
    res.render('index.html');
});


app.use('/', index);

//Ajout du Port
const port = process.env.PORT || '3000';
app.set('port', port);

const serveur = http.createServer(app);

serveur.listen(port, () => console.log(`Running on localhost:${port}`));