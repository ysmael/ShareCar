const express = require('express');
const router = express.Router();
const jwt = require('jsonwebtoken');
const Trajet = require('../models/trajet');
const config = require('../config/database');
const passport = require('passport');



// Response handling
let response = {
    status: 200,
    data: [],
    message: null

};

//sauvegarder un trajet
router.post('/trajets/ajout',function (req,res) {

let newTrajet= Trajet({

depart:req.body.depart,
destination:req.body.destination,
    distance:req.body.distance,
    tempsMoyen:req.body.tempsMoyen,
   prixReference:req.body.prixReference

});

Trajet.addTrajet(newTrajet,function (err,trajet) {
    if(err){
        res.json({
            isSuccess: 0,
            message: "Ajout du trajet a échoué, veuillez réessayer SVP",
            trajet:trajet
        });
    }
    else{

        res.json({
            isSuccess: 1,
            message: "Trajet ajouté"
        });
}

});
});

//recuperer les trajets
router.get('/trajets', function(req, res) {

    Trajet.getAllTrajet(function (err, trajets) {
        if (err) throw err;
        res.send(trajets);
    });

});

    router.post('/trajets/getByDepartDestination',function (req,res) {

    Trajet.getTrajetByDepartDestination(req.body.depart, req.destination,function (err, trajets) {
        if (err) throw err;
        res.send(trajets);
    });

});


    router.post('/trajets/update',function (req,res) {
        var conditions={
            depart:req.body.depart,
            destination:req.destination
        };

        var update={
            distance:req.body.distance,
            tempsMoyen:req.body.tempsMoyen
        }
        options = { multi: true };

        Trajet.updateTrajet(conditions,update,options,function (err, numAffected) {
            if (err){
                res.json({
                    isSuccess: 0,
                    message: "Le trajet n'a pas pu être modifier, veuillez réessayer ultérieurement SVP"
                });
            }
            else
            {
                res.json({
                    isSuccess: 1,
                    message: "Trajet modifié"
                });
            }
        })
    });

router.post('/trajets/delete',function (req,res) {

    let deleteTrajet= Trajet({

        depart:req.body.depart,
        destination:req.body.destination,
        distance:req.body.distance,
        tempsMoyen:req.body.tempsMoyen

    });

    Trajet.supprimerTrajet(deleteTrajet, function (err, trajet) {
        if (!trajet){
            res.json({
                isSuccess: 0,
                message: "Le trajet n'a pas pu être supprimé, veuillez réessayer ultérieurement SVP"
            });
        }
        else
        {
            res.json({
                isSuccess: 1,
                message: "Trajet supprimé"
            });
        }
    })
});


module.exports = router;