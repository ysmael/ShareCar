const express = require('express');
const router = express.Router();
const jwt = require('jsonwebtoken');
const TrajetMembre = require('../models/trajetMembre');
const config = require('../config/database');
const passport = require('passport');



// Response handling
let response = {
    status: 200,
    data: [],
    message: null

};

//sauvegarder un trajet
router.post('/trajetsMembre/ajout',function (req,res) {


    let newTrajetMembre = TrajetMembre({

        depart: req.body.depart,
        destination: req.body.destination,
        date: req.body.date,
        adresseDepart: req.body.adresseDepart,
        adresseArrivee: req.body.adresseArrivee,
        typeVoiture: req.body.typeVoiture,
        nbPlace: req.body.nbPlace,
        nbPlaceDisponible: req.body.nbPlace,
        prix: req.body.prix,
        idMembre: req.body.idMembre,
        nomMembre: req.body.nomMembre,
        prenomMembre: req.body.prenomMembre,
        sexeMembre: req.body.sexeMembre,
        emailMembre: req.body.emailMembre,
        dateNaissanceMembre: req.body.dateNaissanceMembre

    });
    let testTrajet= {
        depart: req.body.depart,
        destination: req.body.destination,
        prix: req.body.prix
    };
    TrajetMembre.testIfTrajetExist(testTrajet, function (err, trajet) {
        if (!trajet) {
            res.json({
                isSuccess: 0,
                message: "Votre trajet ne correspond à aucun trajet type ou le prix est supérieur au prix de référence, veuillez réessayer SVP"
            });
        }
        else {
            TrajetMembre.addTrajetMembre(newTrajetMembre, function (err, trajet) {
                if (err) {
                    res.json({
                        isSuccess: 0,
                        message: "Ajout du trajet Membre a échoué, veuillez réessayer SVP"
                    });
                }
                else {

                    res.json({
                        isSuccess: 1,
                        message: "Trajet ajouté"
                    });
                }

            });
        }
    });
});


//recuperer les trajets
router.get('/trajetsMembre', function(req, res) {
    TrajetMembre.getAllTrajetMembre(function (err, trajets) {
    if (err) throw err;
    res.send(trajets);
});

});



router.post('/trajetsMembre/getTrajet',function (req,res) {
    let trajetRequest= {

        depart:req.body.depart,
        destination:req.body.destination,
        date:req.body.date,
        nbPlaceDisponible: { $gt: 0 },


    };
    TrajetMembre.getTrajet(trajetRequest,function (err, trajets) {
        if (err) throw err;
        console.log(trajetRequest);
        res.send(trajets);
    });

});






router.post('/trajetsMembre/getTrajetPropose',function (req,res) {
let requete={
    idMembre: req.body.idMembre,
};

TrajetMembre.getTrajetProposes(requete,function (err, trajets) {
    if (err) throw err;
    console.log(requete);
    res.send(trajets);
});
});

router.post('/trajetsMembre/getTrajetParticipe',function (req,res) {
    let requete={
        participants:{$elemMatch: {_id:req.body.idMembre}},
    };

    TrajetMembre.getTrajetParticipes(requete,function (err, trajets) {
        if (err) throw err;
        console.log(requete);
        res.send(trajets);
    });
});




router.post('/trajetsMembre/addParticipant',function (req,res) {
    var conditions = {
        _id: req.body.idTrajet,
        nbPlaceDisponible: { $gt: 0 }
    };



    var update = {
        $push: {participants: req.body.participant},
        $inc: {nbPlaceDisponible:-1}
    };
    options = {multi: true};


    TrajetMembre.addParticipantTrajet(conditions, update, options, function (err, numaffected) {
        if (err) {
            res.json({
                isSuccess: 0,
                message: "Reservation non effectué, veuillez réessayer ultérieurement SVP"
            });
        }
        else {
            res.json({
                isSuccess: 1,
                message: "Reservation éffectué"
            });
        }

    });

});

router.post('/trajetsMembre/supprimerTrajet', function (req, res) {
    let trajet= {

        _id:req.body.idTrajet,


    };

    TrajetMembre.supprimerTrajet(trajet,function (err, numaffected) {
        if (err) {
            res.json({
                isSuccess: 0,
                message: "Trajet non supprimé, veuillez réessayer ultérieurement SVP"
            });
        }
        else {
            res.json({
                isSuccess: 1,
                message: "Trajet supprimé"
            });
        }
    });
});

router.post('/trajetsMembre/supprimerParticipation',function (req,res) {
    var conditions = {
        _id: req.body.idTrajet,

    };



    var update = {
        $pull: {participants: { _id:req.body.idParticipant}},
        $inc: {nbPlaceDisponible:1}
    };
    options = {multi: true};


    TrajetMembre.supprimerParticipation(conditions, update, options, function (err, numaffected) {
        if (err) {
            res.json({
                isSuccess: 0,
                message: "Nous n'avous pas pu supprimer votre participation à ce trajet, veuillez réessayer ultérieurement SVP"
            });
        }
        else {
            res.json({
                isSuccess: 1,
                message: "Annulation éffectuée"
            });
        }

    });

});




module.exports = router;