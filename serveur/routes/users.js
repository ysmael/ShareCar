const express = require('express');
const router = express.Router();
const jwt = require('jsonwebtoken');
const User = require('../models/user');
const config = require('../config/database');
const passport = require('passport');


// Error handling
const sendError = (err, res) => {
    response.status = 501;
    response.message = typeof err == 'object' ? err.message : err;
    res.status(501).json(response);
};

// Response handling
let response = {
    status: 200,
    data: [],
    message: null

};

let responseLogin ={
    message:null,
    isSuccess:null
}

let responseRegister ={
    message:null,
    isSuccess: null
}

//recuperer les utilisateurs
/*router.get('/users', (req, res) => {

    userCollection.find()
        .toArray()
        .then((users) => {
        response.data = users;
    res.json(response);});
});
*/

//recuperer les utilisateurs membres
router.get('/users/membres', function(req, res)  {

    User.getAllUser(function (err,users){
    if (err) throw err;
    res.send(users);
});

});

//sauvegarder un utilisateur
router.post('/users/inscription',function (req,res) {



    let newUser = new User({
        nom: req.body.nom,
        prenom:req.body.prenom,
        sexe:req.body.sexe,
        email: req.body.email,
        mdp:req.body.mdp,
        dateNaissance: req.body.dateNaissance,
        "profil":"membre",
        "desactiverTemp":false
    });

        User.addUser(newUser, function(err, user){
            if(err){
                responseRegister.isSuccess = 0;
                responseRegister.message="Inscription echoué";

                res.json(responseRegister);
            }
            else
                {
                    responseRegister.isSuccess = 1;
    responseRegister.message="Inscription réussi";
    res.json(responseRegister);
}
        });



   });

//supprimmer un utilisateur definitivement
router.post('/users/membres/supprimerD', function (req, res) {
    User.supprimerUserD(req.body.id,  function (err) {
        if (err){
            res.json({
                isSuccess: 0,
                message: "Le compte n'a pas pu être supprimé, veuillez réessayer ultérieurement SVP"
            });
        }
        else
        {
            res.json({
                isSuccess: 1,
                message: "Compte supprimé"
            });
        }
    })
});

//supprimer un compte temporairement

router.post('/users/membres/supprimerT',function (req,res) {
    var conditions={
        _id:req.body.id

    };

    var update={
        desactiverTemp:true
    }
    options = { multi: true };

    User.supprimerUserT(conditions,update,options,function (err, numAffected) {
        if (err){
            res.json({
                isSuccess: 0,
                message: "Le compte n'a pas pu être supprimer temporairement, veuillez réessayer ultérieurement SVP"
            });
        }
        else
        {
            res.json({
                isSuccess: 1,
                message: "Compte supprimer temporairement"
            });
        }
    })
});

router.post('/users/connection', function (req, res) {
    const email = req.body.email;
    const mdp= req.body.mdp;
    console.log(req.body.email);

    User.getUserByEmailConnection(email, function(err, user) {
if (err) throw err;
if(!user){
    responseLogin.isSuccess = 0;
    responseLogin.message="Utilisateur non trouvé";
    res.json(responseLogin);
}

User.comparePassword(mdp, user.mdp, function(err, isMatch){

    if (err) throw err;
        if (isMatch) {
            console.log(user);
            const token = jwt.sign({data: user}, config.secret, {
                expiresIn: 604800
            });

            res.json({
                isSuccess: 1,
                message: "Connexion reussi!!",
                token: 'JWT ' + token,
                user: {
                    _id: user._id,
                    nom: user.nom,
                    prenom: user.prenom,
                    email: user.email,
                    sexe: user.sexe,
                    dateNaissance: user.dateNaissance,
                    profil: user.profil
                }
            });
        } else {
            responseLogin.isSuccess = 0;
            responseLogin.message = "Mauvais mot de passe";
            res.json(responseLogin);
        }
    });

    });

});


// Profile
router.get('/users/profile', passport.authenticate('jwt', {session:false}), function(req, res){
    res.json({user: req.user});
});


//modifier un utilisateur


module.exports = router;