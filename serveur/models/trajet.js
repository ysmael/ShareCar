const mongoose = require('mongoose');
const bcrypt = require('bcryptjs');
const config = require('../config/database');

// Trajet Schema
const TrajetSchema = mongoose.Schema({



    depart: {
        type: String,
        required: true
    },
    destination: {
        type: String,
        required: true
    },
    distance: {
        type: String,
        required: true
    },
    tempsMoyen: {
        type: String,
        required: true
    },
    prixReference:{
        type: Number,
        required: true
    }
});


const Trajet = module.exports = mongoose.model('trajets', TrajetSchema);


module.exports.getTrajetByDepartDestination = function(depart,destination, callback){
    const query = {depart: depart, destination:destination};
    Trajet.findOne(query, callback);
};
module.exports.getTrajetByDepartDestinationPrix = function(depart,destination,prix ,callback){
    const query = {depart: depart, destination:destination , prixReference:{$gt :prix}};
    console.log(query);
    Trajet.findOne(query, callback);
};

module.exports.addTrajet = function(newTrajet, callback){
    newTrajet.save(callback);


};

module.exports.getAllTrajet= function( callback){
    Trajet.find(callback);
};

module.exports.supprimerTrajetByDepartDestination= function (depart,destination,callback){
    const query = {depart: depart, destination:destination};
    Trajet.deleteOne(query,callback);

};

module.exports.updateTrajet=function (conditions,update, options,callback){
    Trajet.update(conditions,update,options,callback);
};

module.exports.supprimerTrajet= function (trajet,callback){
    const query = {depart: trajet.depart, destination:trajet.destination};
    console.log(query);
    Trajet.remove(query,callback);


};