const mongoose = require('mongoose');
const bcrypt = require('bcryptjs');
const config = require('../config/database');

// User Schema
const UserSchema = mongoose.Schema({



    nom: {
        type: String,
        required: true
    },
    prenom: {
        type: String,
        required: true
    },
    sexe: {
        type: String,
        required: true
    },
    email: {
        type: String,
        required: true
    },
mdp: {
    type: String,
        required: true
},
dateNaissance: {
    type: Date,
        required: true
},
profil: {
    type: String,
        required: true
},

desactiverTemp: {
    type: Boolean,
    required: true
}

});

const User = module.exports = mongoose.model('users', UserSchema);

module.exports.getUserById = function(id, callback){
    User.findById(id, callback);
};

module.exports.getUserByEmail = function(email, callback){
    const query = {email: email};
    console.log("model User"+email);
    User.findOne(query, callback);
};

module.exports.getUserByEmailConnection = function(email, callback){
    const query = {email: email,desactiverTemp:false};
    console.log("model User"+email);
    User.findOne(query, callback);
};

module.exports.addUser = function(newUser, callback){
    bcrypt.genSalt(10, function(err, salt){
        bcrypt.hash(newUser.mdp, salt, function(err, hash)  {
        if(err) throw err;
    newUser.mdp = hash;
    newUser.save(callback);
});
});
};

module.exports.comparePassword = function(candidatePassword, hash, callback){

    bcrypt.compare(candidatePassword, hash,function (err, isMatch){
        if(err) throw err;
    callback(null, isMatch);
});
};

module.exports.getAllUser= function( callback){
    User.find(callback);
};

//supprimer definitivement un user
module.exports.supprimerUserD= function( userId, callback){

    const query = {_id: userId};
    User.deleteOne(query,callback);
};


//supprimer compte temporairement
module.exports.supprimerUserT =function (conditions,update, options,callback){
   User.update(conditions,update,options,callback);
};