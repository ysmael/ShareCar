const mongoose = require('mongoose');
const bcrypt = require('bcryptjs');
const config = require('../config/database');
const trajet =require('../models/trajet');
// Trajet Schema
const TrajetMembreSchema = mongoose.Schema({



    depart: {
        type: String,
        required: true
    },
    destination: {
        type: String,
        required: true
    },
    date: {
        type: Date,
        required: true
    },
    adresseDepart: {
        type: String,
        required: true
    },
    adresseArrivee: {
        type: String,
        required: true
    },
    typeVoiture: {
        type: String,
        required: true
    },
    nbPlace: {
        type: Number,
        required: true
    },
    nbPlaceDisponible: {
        type: Number,
        required: true
    },
    prix: {
        type: Number,
        required: true
    },
    idMembre:
        {
            type: String,
            required: true
        },
        nomMembre:{
            type: String,
            required: true
        },
    prenomMembre:{
        type: String,
        required: true
    },
    sexeMembre: {
        type: String,
        required: true
    },
    emailMembre: {
        type: String,
        required: true
    },
    dateNaissanceMembre: {
        type: Date,
        required: true
    },
    participants : {
        type: [],
        required: true
    }

});

const TrajetMembre = module.exports = mongoose.model('trajetsMembre', TrajetMembreSchema);

module.exports.getAllTrajetMembre= function( callback){

    TrajetMembre.find(callback);
};

module.exports.testIfTrajetExist = function(newTrajet, callback){
    trajet.getTrajetByDepartDestinationPrix(newTrajet.depart,newTrajet.destination,newTrajet.prix ,callback);
};

module.exports.addTrajetMembre = function(newTrajet, callback){

    newTrajet.save(callback);


};

module.exports.updateTrajetMembre=function (conditions,update, options,callback){
    TrajetMembre.update(conditions,update,options,callback);
};

module.exports.supprimerTrajet= function (trajet,callback){
    TrajetMembre.remove(trajet,callback);

};

module.exports.supprimerParticipation= function (conditions,update, options,callback){
    TrajetMembre.update(conditions,update,options,callback);
};

module.exports.getTrajet=function(trajetsRequest,callback){


    console.log(trajetsRequest);
    TrajetMembre.find(trajetsRequest,callback);
};

module.exports.getTrajetProposes = function(requete,callback){
    console.log(requete);
    TrajetMembre.find(requete,callback);
};

module.exports.addParticipantTrajet = function (conditions,update, options,callback){
    TrajetMembre.update(conditions,update,options,callback);
};

module.exports.getTrajetParticipes = function(requete,callback){
    console.log(requete);
    TrajetMembre.find(requete,callback);
};